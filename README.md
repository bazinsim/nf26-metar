# nf26-metar
Group TD1_9-10
BAZIN Simon - ROISIN Oscar

## Files
### Loading.py
Create tables and load data

### History.py
Objective 1

### Map.py
Objective 2

### Kmeans.py
Objective 3

### Asos.txt
Data for Finland from 2001 to 2010

### Projet.pdf
Project instruction file