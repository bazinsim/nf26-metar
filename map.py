import folium
from cassandra.cluster import Cluster

from kmeans import verifyDateInPeriod
import loading

table_name_space = loading.table_name_space
table_name_date = loading.table_name_date
numeric_columns = loading.numeric_columns.copy()
numeric_columns.remove('lon')
numeric_columns.remove('lat')

FIRST_DAY = loading.FIRST_DAY
LAST_DAY = loading.LAST_DAY

'''
name: getDailyIndicator
description: verify date and return data from database
parameters:
    * year: year in yyyy format
    * month: month in mm format
    * day: day in dd format
    * indicators: list of indicators
return: list of dictionnary, a dictionnary for each station
'''
def getDailyIndicator(year, month, day, indicators):
    # Verify if date exists in the database
    if verifyDateInPeriod(FIRST_DAY, LAST_DAY, year, month, day):
        # Format indicators for request
        ind = list(map(lambda i: f", avg({i}) as {i}", indicators))
        datas = session.execute(f"SELECT station, avg(lat) as lat, avg(lon) as lon {''.join(ind)} FROM {table_name_date} WHERE year = {year} AND month = {month} AND day = {day} GROUP BY year, month, day, station")
        # Reset indicators
        ind = indicators.copy()
        ind.insert(0, 'lon')
        ind.insert(0, 'lat')
        ind.insert(0, 'station')
        # Create list of dict
        result = []
        for row in datas:
            rd = dict()
            n = 0
            for elem in row:
                rd[ind[n]]=elem
                n += 1
            result.append(rd)
        return result
    else:
        return []

'''
name: mapIndicator
description: save a map with stations' indicators for the chosen date
parameters:
    * date : date in yyyy-mm-dd format
    * indicators : list of indicators 
'''
def mapIndicator(date, indicators):
    # Split date and filter indicators
    year, month, day = date.split('-')
    indicators = list(filter(lambda i: i in numeric_columns, indicators))
    # Get data for date and filtered indicators
    data = getDailyIndicator(year, month, day, indicators)
    # Verify if date components are integers
    year = int(year)
    month = int(month)
    day = int(day)
    # Test if data for the date selected
    if not(data):
        print(f"Aucune donnée pour pour le jour : {year}-{month}-{day}")
        return
    else:
        file_name = f"map_for_{year}-{month}-{day}.html"
        # Create map
        m = folium.Map(location=[64.2815, 27.6753], zoom_start = 5)
        # Add Marker for each station
        for d in data:
            text = ''
            for key, value in d.items():
                text = text+'<br><b>'+key+'</b> : '+str(value)
            folium.Marker([d['lat'], d['lon']], popup=f"{text}").add_to(m)
        # Save map
        m.save(file_name)
        print(f"La carte a été enregistrée à {file_name}")

if __name__ == '__main__':
    cluster = Cluster()
    session = cluster.connect()
    session.set_keyspace("bazinsim_roisinos_metar")
    print()
    mapIndicator('2010-03-10', numeric_columns)
    print()