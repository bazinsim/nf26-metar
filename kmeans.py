from cassandra.cluster import Cluster
from datetime import datetime

from sklearn.cluster import KMeans
import numpy as np
import folium

import loading as l
import history as h

colours = ['blue', 'red', 'green', 'orange', 'pink', 'white', 'purple', 'gray']

'''
name: getDatasForPeriod
description: Query the database and get the values for a period for indicators
parameters:
    * startPeriod: beginning date of period to select
    * endPeriod: ending date of period to select
    * indicators: list of indicators to select
return: result of the query
'''
def getDatasForPeriod(startPeriod, endPeriod, indicators):
    datas = []
    for i in range(int(startPeriod[0:4]), int(endPeriod[0:4]) + 1):
        datas += session.execute(f"SELECT year, month, day, station, lat, lon, {indicators} FROM {l.table_name_date} where year = {i}")

    return datas

'''
name: verifyDateInPeriod
description: Verify that the date given is within the period of study
parameters:
    * startPeriod: beginning date of period to select
    * endPeriod: ending date of period to select
    * year: year given by user
    * month: month given by user
    * day: day given by user
return: boolean indicating the validity
'''
def verifyDateInPeriod(startPeriod, endPeriod, year, month, day):
    isDate = year.isdigit() and month.isdigit() and day.isdigit()
    if isDate:
        date = datetime.strptime(year + "-" + month + "-" + day, "%Y-%m-%d")
        dateStart = datetime.strptime(startPeriod, "%Y-%m-%d")
        dateEnd = datetime.strptime(endPeriod, "%Y-%m-%d")
        if date >= dateStart and date <= dateEnd:
            return True
    return False

'''
name: getDecileForAllStations
description: Compute the decile of a list for different stations and indicators
parameters:
    * startPeriod: beginning date of period to select
    * endPeriod: ending date of period to select
    * table: list of lists with all values
    * nb_indicators: number of indicators to compute
    * indicators_list: list of names of indicators to compute
return: a dictionary with lists of dictionaries of lists containing the deciles for indicators for stations
'''
def getDecileForAllStations(startPeriod, endPeriod, table, nb_indicators, indicators_list):
    # map with station and list of maps
    # the list of maps is used for all indicators
    # the second map contains the indicator with the list of values for this indicator
    l = {}
    for t in table:
        if verifyDateInPeriod(startPeriod, endPeriod, str(t[0]), str(t[1]), str(t[2])):
            if t[3] not in l.keys():
                l[t[3]] = []
                for i in range(nb_indicators):
                    if t[6 + i] != None:
                        l[t[3]].append({indicators_list[i] : [float(t[6 + i])]})
            else:
                for i in range(nb_indicators):
                    if t[6 + i] != None:
                        l[t[3]][i][indicators_list[i]].append(float(t[6 + i]))

    # Sort all lists of values
    for station in l.keys():
        for i in range(nb_indicators):
            l[station][i][indicators_list[i]].sort()

    # Deciles is a map mapping station with a list of maps containing indicators and their deciles
    # example for 2 stations with 2 indicators
    # {'EFKI': [{'tmpf': [-23.8, 6.8, 17.6, 26.6, 32.0, 39.2, 44.6, 48.2, 53.6, 62.6, 91.4]}, {'dwpf': [-31.0, 5.0, 14.0, 24.8, 32.0, 35.6, 39.2, 42.8, 50.0, 55.4, 69.8]}], 'EFHA': [{'tmpf': [-23.8, 6.8, 17.6, 26.6, 32.0, 39.2, 44.6, 48.2, 53.6, 62.6, 91.4]}, {'dwpf': [-31.0, 5.0, 14.0, 24.8, 32.0, 35.6, 39.2, 42.8, 50.0, 55.4, 69.8]}]}
    deciles = {}
    for station in l.keys():
        deciles[station] = []
        for i in range(nb_indicators):
            deciles[station].append({indicators_list[i] : []})
            # Compute deciles, from 0 to 10 (= includes min and max)
            for d in range(11):
                if d == 10:
                    deciles[station][i][indicators_list[i]].append(l[station][i][indicators_list[i]][len(l[station][i][indicators_list[i]]) - 1])
                else:
                    deciles[station][i][indicators_list[i]].append(l[station][i][indicators_list[i]][len(l[station][i][indicators_list[i]]) // 10 * d])

    return deciles

'''
name: applyKmeans
description: Apply k-means algorithm to clusterize space
parameters:
    * deciles: a dictionary with lists of dictionaries of lists containing the deciles for indicators for stations
    * nb_indicators: number of indicators in the deciles
    * indicators_list: list of names of indicators in the deciles
    * startPeriod: beginning date of period to select
    * endPeriod: ending date of period to select
return: a dictionary with the station and its associated cluster
'''
def applyKmeans(deciles, nb_indicators, indicators_list, startPeriod, endPeriod):
    # Create table without map
    table = []
    # Create list with stations name
    stations_name = []

    for station in deciles.keys():
        t = []
        stations_name.append(station)
        for i in range(nb_indicators):
            t += deciles[station][i][indicators_list[i]]
        table.append(t)

    nb_clusters = 4
    if len(stations_name) < nb_clusters:
        print(f"Le nombre de villes ayant des données est trop inférieur ({len(stations_name)}) pour appliquer les kmeans pour la période du {startPeriod} au {endPeriod}")
        return None

    kmeans = KMeans(n_clusters = nb_clusters, max_iter = 100).fit(table)

    res = {}
    i = 0
    for station in stations_name:
        res[station] = kmeans.labels_[i]
        i += 1

    return res


'''
name: kmeans
description: Clusterize space for a period depending on deciles and create a map of the country
parameters:
    * startPeriod: beginning date of period to select
    * endPeriod: ending date of period to select
    * indicators_list: list of names of indicators to take in account
'''
def kmeans(startPeriod, endPeriod, indicators_list):
    startDate = datetime.strptime(startPeriod, "%Y-%m-%d")
    endDate = datetime.strptime(endPeriod, "%Y-%m-%d")

    firstDate = datetime.strptime(l.FIRST_DAY, "%Y-%m-%d")
    lastDate = datetime.strptime(l.LAST_DAY, "%Y-%m-%d")

    if startDate < firstDate or startDate > lastDate or endDate < firstDate or endDate > lastDate:
        print(f"Les dates doivent être comprises entre {l.FIRST_DAY} et {l.LAST_DAY}")
        return

    if not h.verifyYearValidity(int(startPeriod[0:4]), int(endPeriod[0:4])):
        return

    # Create a string with indicators concatenated
    indicators = ""
    indicators_list_numeric = []
    nb_indicators = 0
    for ind in indicators_list:
        if ind in l.numeric_columns:
            if nb_indicators == 0:
                indicators += ind
                indicators_list_numeric.append(ind)
                nb_indicators += 1
            else:
                indicators += "," + ind
                indicators_list_numeric.append(ind)
                nb_indicators += 1

    table = getDatasForPeriod(startPeriod, endPeriod, indicators)
    table = list(table)
    # Get coordinates
    coord = dict()
    for t in table:
        coord[t[3]]=(t[4], t[5])
    # Get the map with all deciles for all stations and indicators
    table_deciles = getDecileForAllStations(startPeriod, endPeriod, table, nb_indicators, indicators_list_numeric)

    station_with_center = applyKmeans(table_deciles, nb_indicators, indicators_list_numeric, startPeriod, endPeriod)
    if station_with_center != None:
        file_name = f"map_kmeans_{startPeriod}_to_{endPeriod}.html"
        # Create map
        m = folium.Map(location=[64.2815, 27.6753], zoom_start = 5)
        # Add Marker for each station
        for key, value in station_with_center.items():
            folium.Marker([coord[key][0], coord[key][1]], popup=f"<b>{key}</b>", icon=folium.Icon(color=colours[value])).add_to(m)
        # Save map
        m.save(file_name)
        print(f"La carte a été enregistrée à {file_name}")
    else:
        print(f"Aucune clusterisation déterminée")

if __name__ == '__main__':
    cluster = Cluster()
    session = cluster.connect()
    session.set_keyspace("bazinsim_roisinos_metar")

    print()
    # kmeans("2001-01-01", "2010-12-31", ["tmpf", "skyc1"])
    kmeans("2001-01-01", "2010-12-31", ["tmpf", "dwpf", "skyc1"])
    print()
