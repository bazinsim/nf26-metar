import matplotlib.pyplot as plt
from cassandra.cluster import Cluster
from datetime import datetime
import numpy as np
import math

import loading

table_name_space = loading.table_name_space
table_name_date = loading.table_name_date
numeric_columns = loading.numeric_columns

MIN_DATE = loading.MIN_DATE
MAX_DATE = loading.MAX_DATE


'''
name: getHistory
description: Return the datas from the database
parameters:
    * station: station id to restrict on
    * indicator: indicator to select
return: query to create table
'''
def getHistory(station, indicator):
    datas = session.execute(f"SELECT year, month, day, {indicator} FROM {table_name_space} where station = '{station}'")
    return datas

'''
name: getMeanByDay
description: Compute the mean of values by day
parameters:
    * table: list with values
    * dateMin: beginning year of the period to compute
    * dateMax: ending year of the period to compute
return: dictionary with mean with date
'''
def getMeanByDay(table, dateMin, dateMax):
    # Dictionary to store sum of measures and number of measures by day
    table_date = {}
    for r in table:
        year = r[0]
        # Verify the measures is for the period chosen by user and have a value
        if year >= dateMin and year < dateMax and  r[len(r) - 1] != None:
            # convert attributes to date format as string
            date = str(r[0]) + "-" + "0" * (2 - len(str(r[1]))) + str(r[1]) + "-" + "0" * (2 - len(str(r[2]))) + str(r[2])
            if date not in table_date.keys():
                table_date[date] = 0,0
            table_date[date] = (table_date[date][0] + r[len(r) - 1], table_date[date][1] + 1)

    # Treat datas to get mean by day
    for d in table_date.keys():
        table_date[d] = table_date[d][0] / table_date[d][1]

    return table_date

'''
name: getMeanByMonth
description: Compute the mean of values by month for the entire period
parameters:
    * table: list with values
return: dictionary with mean by month
'''
def getMeanByMonth(table):
    # Dictionary to store sum of measures and number of measures by month
    table_month = {}
    for r in table:
        # Verify the value of measure (needs to have a value)
        if r[len(r) - 1] != None:
            month = r[1]
            if month not in table_month.keys():
                table_month[month] = 0,0
            table_month[month] = (table_month[month][0] + r[len(r) - 1], table_month[month][1] + 1)

    # Treat datas to get mean by month
    for d in table_month.keys():
        table_month[d] = table_month[d][0] / table_month[d][1]

    return table_month

'''
name: verifyYearValidity
description: Verify the validity of the year given
parameters:
    * dateMin: first year
    * dateMax: last year
return: boolean indicating the validity
'''
def verifyYearValidity(dateMin, dateMax):
    # Verification to ensure the validity of parameters, dates not equal
    if dateMin == dateMax:
        print(f"Les dates ne doivent pas être égales")
        return False

    # Verification to ensure the validity of parameters, dates in the right period
    if dateMin < MIN_DATE or dateMin > (MAX_DATE + 1) or dateMax < MIN_DATE or dateMax > (MAX_DATE + 1):
        print(f"Les dates doivent être comprises entre {MIN_DATE} et {MAX_DATE}")
        return False
    return True

'''
name: plotHistory
description: Plot the curves of evolution and seasonality and save it
parameters:
    * station: station to analyse
    * indicator: indicator to analyse evolution
    * dateMin: first year of the period to plot
    * dateMax: last year of the period to plot
'''
def plotHistory(station, indicator, dateMin, dateMax):
    dateMax = dateMax + 1
    # Accept only indicator with numeric values (not factors)
    if indicator in numeric_columns:
        if not verifyYearValidity(dateMin, dateMax):
            return

        # Get datas from cassandra table
        table = getHistory(station, indicator)
        table = list(table)

        # If no data for the period selected
        if not table:
            print(f"Aucune donnée pour la station {station} et pour l'indicateur {indicator} et pour la période {dateMin} - {dateMax}")
            return

        # Treat datas
        table_mean = getMeanByDay(table, dateMin, dateMax)
        table_mean_by_month = getMeanByMonth(table)
        if not table_mean or not table_mean_by_month:
            print(f"Aucune donnée pour la station {station} et pour l'indicateur {indicator} et pour la période {dateMin} - {dateMax}")
            return

        # Duplicate list for each year in the period required
        liste = []
        for i in range(dateMax - dateMin):
            for key,value in table_mean_by_month.items():
                liste.append([key, value])

        # Completing the month to have a date format (yyyy-month-01)
        i = dateMin
        j = 1
        for k in range(len(liste)):
            j += 1
            liste[k][0] = str(i) + '-' + str(liste[k][0])
            if j > 12:
                i += 1
                j = 1

        # Name for file
        currentDateTime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = str(currentDateTime) + "_" + station + "_" + indicator + ".png"

        # Configure graduation of plot
        # Need to find a nice step for graduation (no more than 6 values to be understandable)
        step_graduation = max(math.ceil((dateMax - dateMin) / 5), 1)
        # Convert the graduation to the date format (yyyy-month-01)
        graduation = ["20" + "0" * (2 - len(str(i))) + str(i) + "-01-01" for i in range(int(str(dateMin)[2:4]), int(str(dateMax)[2:4]), step_graduation)]

        # Pour ne pas que la date de fin se superpose avec une autre date
        if (dateMax - dateMin) % 2 == 0 or step_graduation == 1:
            # Add the last value of graduation for the last day of measures
            graduation.append("20" + "0" * (2 - len(str(dateMax - 1)[2:4])) + str(dateMax - 1)[2:4] + "-12-31")
        
        # Plot, with both measures and season mean
        fig, ax1 = plt.subplots()
        # Measures on axis 1
        ax1.plot_date(table_mean.keys(), table_mean.values(), '-', xdate = True, label = 'Evolution')
        ax1.xaxis.set_ticks(graduation)
        ax2 = ax1.twiny()
        # Seasonal mean
        ax2.plot([elt[0] for elt in liste], [elt[1] for elt in liste], '-', color = "r", label = 'Saison')
        # Do not show graduation on the top of the plot
        ax2.xaxis.set_ticks([])

        # Set title and labels
        plt.title(f"Evolution de {indicator} pour la station {station}")
        ax1.set_xlabel('Date')
        ax1.set_ylabel(indicator)

        # Legend
        h1, l1 = ax1.get_legend_handles_labels()
        h2, l2 = ax2.get_legend_handles_labels()
        plt.legend(h1+h2, l1+l2, loc='lower right')

        plt.tick_params(
            axis='x',
            which='both',
            bottom=False,
            top=True
        )
        # Save figure
        plt.savefig(file_name)
        print(f"Le graphique a été enregistré à {file_name}")

    else:
        print("Les données pour cet indicateur ne sont pas numériques, impossible de tracer un graphique")

if __name__ == '__main__':
    cluster = Cluster()
    session = cluster.connect()
    session.set_keyspace("bazinsim_roisinos_metar")

    print()
    plotHistory("EFKI", "tmpf", 2001, 2004)
    print()