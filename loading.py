from cassandra.cluster import Cluster
import csv
import re

FILE_NAME = "asos.txt"
table_name_space = "station"
table_name_date = "date"

numeric_columns = ["lon","lat","tmpf","dwpf","relh","drct","sknt","p01i","alti","mslp","vsby","gust","skyl1","skyl2","skyl3","skyl4","feel","ice_accretion_1hr","ice_accretion_3hr","ice_accretion_6hr","peak_wind_gust","peak_wind_drct","peak_wind_time"]

MIN_DATE = 2001
MAX_DATE = 2010

FIRST_DAY = "2001-01-01"
LAST_DAY = "2010-12-31"

# Country: Finland
# Dates : 2001 to 2010

## datas format:
# station: three or four character site identifier
# valid: timestamp of the observation
# lon: longitude of the station
# lat: latitude of the station
# tmpf: Air Temperature in Fahrenheit, typically @ 2 meters
# dwpf: Dew Point Temperature in Fahrenheit, typically @ 2 meters
# relh: Relative Humidity in %
# drct: Wind Direction in degrees from north
# sknt: Wind Speed in knots
# p01i: One hour precipitation for the period from the observation time to the time of the previous hourly precipitation reset. This varies slightly by site. Values are in inches. This value may or may not contain frozen precipitation melted by some device on the sensor or estimated by some other means. Unfortunately, we do not know of an authoritative database denoting which station has which sensor.
# alti: Pressure altimeter in inches
# mslp: Sea Level Pressure in millibar
# vsby: Visibility in miles
# gust: Wind Gust in knots
# skyc1: Sky Level 1 Coverage
# skyc2: Sky Level 2 Coverage
# skyc3: Sky Level 3 Coverage
# skyc4: Sky Level 4 Coverage
# skyl1: Sky Level 1 Altitude in feet
# skyl2: Sky Level 2 Altitude in feet
# skyl3: Sky Level 3 Altitude in feet
# skyl4: Sky Level 4 Altitude in feet
# wxcodes: Present Weather Codes (space seperated)
# feel: Apparent Temperature (Wind Chill or Heat Index) in Fahrenheit
# ice_accretion_1hr: Ice Accretion over 1 Hour (inches)
# ice_accretion_3hr: Ice Accretion over 3 Hours (inches)
# ice_accretion_6hr: Ice Accretion over 6 Hours (inches)
# peak_wind_gust: Peak Wind Gust (from PK WND METAR remark) (knots)
# peak_wind_drct: Peak Wind Gust Direction (from PK WND METAR remark) (deg)
# peak_wind_time: Peak Wind Gust Time (from PK WND METAR remark)
# metar: unprocessed reported observation in METAR format

'''
name: loadata
description: Load datas of the file, by creating a generator
parameters:
    * filename: File name to load
return: generator of dictionaries with value and name of data
'''
def loadata(filename):
    dateparser = re.compile(
        "(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)"
    )
    with open(filename) as f:
        for r in csv.DictReader(f):
            data = {}
            data["station"] = r["station"]
            valid = dateparser.match(r["valid"]).groupdict()

            data["year"] = int(valid["year"])
            data["month"] = int(valid["month"])
            data["day"] = int(valid["day"])
            data["hour"] = int(valid["hour"])
            data["minute"] = int(valid["minute"])

            data["lon"] = float(r["lon"])
            data["lat"] = float(r["lat"])

            if r["tmpf"] == 'null':
                data["tmpf"] = 'null'
            else:
                data["tmpf"] = float(r["tmpf"])

            if r["dwpf"] == 'null':
                data["dwpf"] = 'null'
            else:
                data["dwpf"] = float(r["dwpf"])

            if r["relh"] == 'null':
                data["relh"] = 'null'
            else:
                data["relh"] = float(r["relh"])

            if r["drct"] == 'null':
                data["drct"] = 'null'
            else:
                data["drct"] = float(r["drct"])

            if r["sknt"] == 'null':
                data["sknt"] = 'null'
            else:
                data["sknt"] = float(r["sknt"])

            if r["p01i"] == 'null':
                data["p01i"] = 'null'
            else:
                data["p01i"] = float(r["p01i"])	

            if r["alti"] == 'null':
                data["alti"] = 'null'
            else:
                data["alti"] = float(r["alti"])

            if r["mslp"] == 'null':
                data["mslp"] = 'null'
            else:
                data["mslp"] = float(r["mslp"])

            if r["vsby"] == 'null':
                data["vsby"] = 'null'
            else:
                data["vsby"] = float(r["vsby"])	

            if r["gust"] == 'null':
                data["gust"] = 'null'
            else:
                data["gust"] = float(r["gust"])

            data["skyc1"] = r["skyc1"]
            data["skyc2"] = r["skyc2"]
            data["skyc3"] = r["skyc3"]
            data["skyc4"] = r["skyc4"]

            if r["skyl1"] == 'null':
                data["skyl1"] = 'null'
            else:
                data["skyl1"] = float(r["skyl1"])

            if r["skyl2"] == 'null':
                data["skyl2"] = 'null'
            else:
                data["skyl2"] = float(r["skyl2"])

            if r["skyl3"] == 'null':
                data["skyl3"] = 'null'
            else:
                data["skyl3"] = float(r["skyl3"])

            if r["skyl4"] == 'null':
                data["skyl4"] = 'null'
            else:
                data["skyl4"] = float(r["skyl4"])

            data["wxcodes"] = r["wxcodes"]

            if r["feel"] == 'null':
                data["feel"] = 'null'
            else:
                data["feel"] = float(r["feel"])

            if r["ice_accretion_1hr"] == 'null':
                data["ice_accretion_1hr"] = 'null'
            else:
                data["ice_accretion_1hr"] = float(r["ice_accretion_1hr"])

            if r["ice_accretion_3hr"] == 'null':
                data["ice_accretion_3hr"] = 'null'
            else:
                data["ice_accretion_3hr"] = float(r["ice_accretion_3hr"])

            if r["ice_accretion_6hr"] == 'null':
                data["ice_accretion_6hr"] = 'null'
            else:
                data["ice_accretion_6hr"] = float(r["ice_accretion_6hr"])

            if r["peak_wind_gust"] == 'null':
                data["peak_wind_gust"] = 'null'
            else:
                data["peak_wind_gust"] = float(r["peak_wind_gust"])

            if r["peak_wind_drct"] == 'null':
                data["peak_wind_drct"] = 'null'
            else:
                data["peak_wind_drct"] = float(r["peak_wind_drct"])

            if r["peak_wind_time"] == 'null':
                data["peak_wind_time"] = 'null'
            else:
                data["peak_wind_time"] = float(r["peak_wind_time"])

            data["metar"] = r["metar"]
            
            yield data

NAME_COLUMNS = """
    station 		  ,
    year			  ,
    month 			  ,
    day 			  ,
    hour 			  ,
    minute 			  ,
    lon 			  ,
    lat 			  ,
    tmpf 			  ,
    dwpf 			  ,
    relh 			  ,
    drct 			  ,
    sknt 			  ,
    p01i 			  ,
    alti 			  ,
    mslp 			  ,
    vsby 			  ,
    gust 			  ,
    skyc1 			  ,
    skyc2 			  ,
    skyc3 			  ,
    skyc4 			  ,
    skyl1 			  ,
    skyl2 			  ,
    skyl3 			  ,
    skyl4 			  ,
    wxcodes           ,
    feel              ,
    ice_accretion_1hr ,
    ice_accretion_3hr ,
    ice_accretion_6hr ,
    peak_wind_gust    ,
    peak_wind_drct    ,
    peak_wind_time    ,
    metar             
    """

'''
name: dropTableQuery
description: Create a cql query to drop the table
parameters:
    * table: Table name to drop
return: query to drop table
'''
def dropTableQuery(table):
    return f"""
        DROP TABLE IF EXISTS {table}
    """

'''
name: createTableQuery
description: Create a cql query to create a table with attributes, partitionned by station
parameters:
    * table: Table name to create
return: query to create table
'''
def createTableQuery(table):
    query = f"""CREATE TABLE {table}(
            station 			varchar,
            year				int,
            month				int,
            day					int,
            hour				int,
            minute				int,
            lon 				decimal,
            lat 				decimal,
            tmpf 				decimal,
            dwpf 				decimal,
            relh 				decimal,
            drct 				decimal,
            sknt 				decimal,
            p01i 				decimal,
            alti 				decimal,
            mslp 				decimal,
            vsby 				decimal,
            gust 				decimal,
            skyc1 				varchar,
            skyc2 				varchar,
            skyc3 				varchar,
            skyc4 				varchar,
            skyl1 				decimal,
            skyl2 				decimal,
            skyl3 				decimal,
            skyl4 				decimal,
            wxcodes           	varchar,
            feel              	decimal,
            ice_accretion_1hr 	decimal,
            ice_accretion_3hr 	decimal,
            ice_accretion_6hr 	decimal,
            peak_wind_gust    	decimal,
            peak_wind_drct    	decimal,
            peak_wind_time    	decimal,
            metar             	varchar,
            PRIMARY KEY((station), year, month, day, hour, minute)
        )"""
    return query


'''
name: createTableQueryPartitionningByDate
description: Create a cql query to create a table with attributes, partitionned by date
parameters:
    * table: Table name to create
return: query to create table
'''
def createTableQueryPartitionningByDate(table):
    query = f"""CREATE TABLE {table}(
            station 			varchar,
            year				int,
            month				int,
            day					int,
            hour				int,
            minute				int,
            lon 				decimal,
            lat 				decimal,
            tmpf 				decimal,
            dwpf 				decimal,
            relh 				decimal,
            drct 				decimal,
            sknt 				decimal,
            p01i 				decimal,
            alti 				decimal,
            mslp 				decimal,
            vsby 				decimal,
            gust 				decimal,
            skyc1 				varchar,
            skyc2 				varchar,
            skyc3 				varchar,
            skyc4 				varchar,
            skyl1 				decimal,
            skyl2 				decimal,
            skyl3 				decimal,
            skyl4 				decimal,
            wxcodes           	varchar,
            feel              	decimal,
            ice_accretion_1hr 	decimal,
            ice_accretion_3hr 	decimal,
            ice_accretion_6hr 	decimal,
            peak_wind_gust    	decimal,
            peak_wind_drct    	decimal,
            peak_wind_time    	decimal,
            metar             	varchar,
            PRIMARY KEY((year), month, day, station, hour, minute)
        )"""
    return query

'''
name: insertQueryData
description: Create a cql query to insert a row 
parameters: 
    * row: values for the attributes
    * table: Table name to create
'''
def insertQueryData(row, table):
    query = f"""
        INSERT INTO
            {table}(
                {NAME_COLUMNS}
            )
            VALUES(
                '{row["station"]}',
                {row["year"]},
                {row["month"]},
                {row["day"]},
                {row["hour"]},
                {row["minute"]},
                {row["lon"]},
                {row["lat"]},
                {row["tmpf"]},
                {row["dwpf"]},
                {row["relh"]},
                {row["drct"]},
                {row["sknt"]},
                {row["p01i"]},
                {row["alti"]},
                {row["mslp"]},
                {row["vsby"]},
                {row["gust"]},
                '{row["skyc1"]}',
                '{row["skyc2"]}',
                '{row["skyc3"]}',
                '{row["skyc4"]}',
                {row["skyl1"]},
                {row["skyl2"]},
                {row["skyl3"]},
                {row["skyl4"]},
                '{row["wxcodes"]}',
                {row["feel"]},
                {row["ice_accretion_1hr"]},
                {row["ice_accretion_3hr"]},
                {row["ice_accretion_6hr"]},
                {row["peak_wind_gust"]},
                {row["peak_wind_drct"]},
                {row["peak_wind_time"]},
                '{row["metar"]}'
            )
        ;
    """
    return query

# Not to execute when importing file
if __name__ == '__main__':
    cluster = Cluster()

    session = cluster.connect()

    session.set_keyspace("bazinsim_roisinos_metar")

    dict = loadata(FILE_NAME)

    # --------------------------------------------------------
    # A faire seulement 1 fois pour charger les données
    session.execute(dropTableQuery(table_name_space))
    print(f"Table {table_name_space} dropped")
    session.execute(createTableQuery(table_name_space))
    print(f"Table {table_name_space} created")

    print(f"Starting inserting datas into table {table_name_space}")
    for d in dict:
    	session.execute(insertQueryData(d, table_name_space))
    print(f"Datas inserted into {table_name_space}")
    # --------------------------------------------------------

    # --------------------------------------------------------
    # A faire seulement 1 fois pour charger les données
    session.execute(dropTableQuery(table_name_date))
    print(f"Table {table_name_date} dropped")
    session.execute(createTableQueryPartitionningByDate(table_name_date))
    print(f"Table {table_name_date} created")

    print(f"Starting inserting datas into table {table_name_date}")
    for d in dict:
        session.execute(insertQueryData(d, table_name_date))
    print(f"Datas inserted into {table_name_date}")
    # --------------------------------------------------------